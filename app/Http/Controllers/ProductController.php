<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    //

	function post(Request $request)
	{
		$product = new Product;
		$product->name = $request->name;
		$product->brand = $request->brand;
		$product->price = $request->price;
		$product->image = $request->image;
		$product->gender = $request->gender;
		$product->category = $request->category;
		$product->quantity = $request->quantity;

		$product->save();

		return response()->json(
			[
				"message" => "Success",
				"data" => $product
			]
		);
	}

	function get()
	{
		$data = Product::all();

		return response()->json(
			[
				"message" => "Success",
				"data" => $data
			]
		);
	}

	function getById($id)
	{
		$data = Product::where('id' , $id)->get();

		return response()->json(
			[
				"message" => "Success",
				"data" => $data
			]
		);
	}

	function put($id, request $request)
	{
		$product = Product::where('id' , $id)->first();
		if ($product) {
			$product->name = $request->name ? $request->name : $product->name;
			$product->brand = $request->brand ? $request->brand : $product->brand;
			$product->price = $request->price ? $request->price : $product->price;
			$product->image = $request->image ? $request->image : $product->image;
			$product->gender = $request->gender ? $request->gender : $product->gender;
			$product->category = $request->category ? $request->category : $product->category;
			$product->quantity = $request->quantity ? $request->quantity : $product->quantity;

			$product->save();
			return response()->json(
				[
					"message" => "PUT METHOD Success",
					"data" => $product

				]
			);
		}

		return response()->json(
			[
				"message" => "PUT METHOD Fail"  . $id
			],400
		);

	}

	function delete($id)
	{
		$product = Product::where('id', $id)->first();
		if ($product) {
			$product->delete();
			return response()->json(
				[
					"message" => "DELETE METHOD Success"  .$id
				]
			);
		}
		return response()->json(
			[
				"message" => "DELETE METHOD Fail"  . $id
			],400
		);
	}
}
