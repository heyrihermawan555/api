<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = "products";

    protected $fillable = ['name','brand','price','image','gender','category','quantity'];
    protected $hidden = ['created_at' , 'updated_at'];

}
